
# Sohaila Mohamed 
## Sophomore Student, Biomedical Engineering, Cairo University

> [sohailamohamed.sbme@gmail.com](sohailamohamed.sbme@gmail.com)

------

### Profile {#profile}

Seeking a challenging internship/training in a local or global organization where my academic background and interpersonal skills are well developed and utilized.

------

### Education {#education}

Bachelor of Engineering, Faculty of Engineering, Cairo University, Giza, Egypt, expected to graduate in 2020.
__Major__: Systems and Biomedical Engineering.

------

### Interests {#interests}

* Origami Designs

* Programing

* Swimming

* Reading novels

* Traveling

------

### Work Experience {#experience}

BEAT
: Cairo University, public relations member
__2017-2018__

BEAT
: Cairo University, vice president
__2018__

BECEM Conference
: Cairo University, public relations Organizer
__2017__

Appx
: Hack your Mind, Zewail City of Science and Technology, (BCI) workshop Manager
__2017__

ISC
: International student conference, Cairo University, Fund raising Organizer
__2017__

------

### Courses {#courses}
								             
* Android
  : K-Vector, 2017

* Embedded Linux
  : Pixels Helwan, 2017

* Presentation Skills
  : Place: BEAT, 2017

* OOP
  : HTC, 2017

* AVR and embedded systems
  : IEEE CUSB, 2017

* Marketing principles
  : American Embassy, 2017

* Arduino
  : Arab African Group, 2016

* Robotics
  : STP, 2016

* C++
  : CECE Laps, 2016

------

### Language Skills {#lskills}

* Arabic
  : Mother Tongue

* English
  : Very Good

------

### Computer Skills {#cskills}

1. C, C++ and OOP

1. Circuits Design: Multisim

1. Embedded systems

1. Software Maintenance

1. Microsoft Office

1. Embedded Linux

------

### Interpersonal Skills {#iskills}

1. Leadership

1. Time Management

------

### Presentation Skills {#pskills}

1. Teamwork

1. Problem Solving